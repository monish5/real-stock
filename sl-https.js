class SlHTTP{
    async manageResponse(response){
        if(response.ok){
            const data=await response.json();
            return data;
        }
        throw new Error("Error Occured : "+response.status);
    }
    
    async get(url){
        // console.log(url);
        const response= await fetch(url);
        return this.manageResponse(response);
    }
    async post(url,data){
        // console.log(url)
            const options={
                method: 'POST',
                headers:{
                    'Content-type':'application/json'
                },
                body: JSON.stringify(data)
            };
            const response= await fetch(url,options);
            return this.manageResponse(response);
            
        }
    async put(url,data){
            const options={
                method: 'PUT',
                headers:{
                    'Content-type':'application/json'
                },
                body: JSON.stringify(data)
            };
        const response=await fetch(url,options);
        return this.manageResponse(response);
    }
    async delete(url){
            const options={
                method: 'DELETE',
                headers:{
                    'Content-type':'application/json'
                },
            };
            const response= await fetch(url,options)
            return this.manageResponse(response);
        }
}

const http = new SlHTTP();
// export default http;