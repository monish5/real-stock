const stockname=localStorage.getItem("stockname");
var mainbal=localStorage.balance;
document.addEventListener("DOMContentLoaded",getlogo(stockname));
const availablebalance=document.getElementById("availablebalance");

document.addEventListener("DOMContentLoaded",()=>{
// console.log(localStorage.getItem("mainbalance"));
availablebalance.innerHTML="$ "+(parseFloat(localStorage.balance)).toFixed(2);

})

document.getElementById("stockName").value=localStorage.getItem("stockname");
const search=document.getElementById("search");
search.addEventListener("click",()=>{
    console.log("aaya search me");
    const val=document.getElementById("stockName");
    if(val.value.length!=0){
        localStorage.setItem("stockname",val.value);
        window.location.reload();
    }
    else{
        alert("Enter a StockName")
    }
})

const eod1=`https://api.twelvedata.com/eod?symbol=`;
const eod2=`&apikey=6d4ca4f75c0647769580036059841fba`;


function getlogo(stockname){
    let image=document.createElement("img");
    if(stockname!="dji" && stockname!="spx" && stockname!="ndx"){

        console.log("stock ka name h "+stockname)
        const sname=stockname.toUpperCase();
        image.src=`https://assets-netstorage.groww.in/intl-stocks/logos/${sname}.png`;
        image.style.width="64px";
        image.style.height="64px";
        document.getElementById("stock-logo").appendChild(image);
    }
    else{
        document.getElementById("stock-logo").innerHTML="";
    }
    console.log(stockname);
    getlive(stockname);
    getname(stockname);
}

async function getlive(stockname){
    const liveurl1="https://api.twelvedata.com/price?symbol=";
    const liveurl2="&apikey=6d4ca4f75c0647769580036059841fba";
    let liveurl=`${liveurl1}${stockname}${liveurl2}`;

    console.log("live me aaya");
    const liveres=await fetch(liveurl);
    const liveresponse= await liveres.json();
    const rate=Number(liveresponse.price).toFixed(2);
    
    const qty=document.getElementById("stockquantity")
    var qnty=qty.valueAsNumber;
    qty.addEventListener("input",()=>{
    qnty=qty.valueAsNumber;
    // let amount=qnty*rate;
    getrbalance(qnty,rate);
    });


    const buy=document.getElementById("buybutton");
    buy.addEventListener("click",(e)=>{
        e.preventDefault();
        if(qnty==0 || qnty==null){
            alert("Atleast add 1 stock")
        }
        buynow(e,stockname,qnty,(qnty*rate).toFixed(2),rate);
    });

    document.getElementById("stockprice").innerHTML="$ " +rate;    
    const a=document.getElementsByTagName("iframe")[0];
    a.outerHTML=`<iframe id="tradingview_c029e" src="https://s.tradingview.com/widgetembed/?frameElementId=tradingview_c029e&amp;symbol=NASDAQ%3A${stockname}&amp;interval=5&amp;symboledit=1&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;studies=%5B%5D&amp;theme=light&amp;style=1&amp;timezone=Etc%2FUTC&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;locale=in&amp;utm_source=127.0.0.1&amp;utm_medium=widget&amp;utm_campaign=chart&amp;utm_term=NASDAQ%3A${stockname}#%7B%22page-uri%22%3A%22127.0.0.1%3A5500%2Fstockpage.html%22%7D" style="width: 100%; height: 100%; margin: 0 !important; padding: 0 !important;" frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen="">
    </iframe>`;
}
function getrbalance(qty,rate){
    // console.log("rb me aaya "+qty);
    let amount=qty*rate;
    console.log(amount+ " amt");
    const fixed=(mainbal/rate).toFixed(0);
    if(qty<=0 || qty==null){
        alert("No negative buying allowed")
        document.getElementById("requiredbalance").innerHTML="$ 0";    
    }
    else if(amount>mainbal){
        var x=document.getElementById("stockquantity").max=fixed;
        console.log("yes bda h")
        document.getElementById("requiredbalance").style.color="red"
    }
    else{
        console.log("direct aaya");
        document.getElementById("requiredbalance").style.color="black"
        document.getElementById("requiredbalance").innerHTML="$ "+(amount).toFixed(2);
        
    }
}
async function getname(stockname){
    console.log("name mke aay");
    document.getElementById("stockname").innerHTML=stockname.toUpperCase()
    document.getElementById("brand-name").innerHTML=stockname.toUpperCase();
    // document.getElementById("nd-sector").innerHTML=resp.sector;
}
// document.getElementById("stockname").innerHTML=document.getElementsByClassName("mainTitle-l31H9iuA")[0].innerHTML;

// valueAsNumber
async function buynow(e,stockname,stockquantity,requiredbalance,stockprice){
    if(stockquantity>(mainbal/stockprice).toFixed(0)){
        alert("max buying allowed "+(mainbal/stockprice).toFixed(0));
    }
    else{

        e.preventDefault();
        // window.location.reload=false;
        console.log("buynow me aaya");
        const data={
            stockname,
            stockquantity,
            requiredbalance,
            stockprice
        };
        http.post("http://localhost:3000/posts",data)
        .then(ans=>{
            ui.clearAlert();
            console.log(ans);
        })
        .catch(err=>console.warn(err));
        
        http.post("http://localhost:3000/transaction",data)
        .then(ans=>{
            ui.showAlert('alert alert-success',`Bought ${stockquantity} of ${stockname} at \$ ${stockprice} for ${requiredbalance}`)
            ui.clearAlert();
        })
        .catch(err=>console.warn(err));
        localStorage.balance=mainbal-requiredbalance
        availablebalance.innerHTML="$ "+mainbal-requiredbalance;
        // localStorage.setItem("mainbalance",localStorage.getItem("mainbalance")-requiredbalance);
    }

    // document.getElementById("stockquantity").innerHTML=0;
}

