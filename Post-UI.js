// import http from "../sl-https";
console.log("hi");
// console.log(http);
const detail_container=document.getElementById("detail-container");
console.log(detail_container);
const stockname=document.getElementById('stockname');
const stockquantity=document.getElementById('stockquantity');
const stockprice=document.getElementById('stockprice');
const requiredbalance=document.getElementById('requiredbalance');

async function getlive(stockname,qty){
    const liveurl1="https://api.twelvedata.com/price?symbol=";
    const liveurl2="&apikey=6d4ca4f75c0647769580036059841fba";
    let liveurl=`${liveurl1}${stockname}${liveurl2}`;
    console.log("live me aaya");
    const liveres=await fetch(liveurl);
    const liveresponse= await liveres.json();
    let rate1=Number(liveresponse.price).toFixed(2);
    // console.log(localStorage.balance);
    let ans=parseFloat(qty)*rate1;
    ans=parseFloat(ans).toFixed(2);
    ans=Number(ans);
    let bal=Number((localStorage.balance));
    localStorage.balance=parseFloat(bal+ans);
}

class UI{
    constructor(){

    }
    
    showPosts(){
        http.get("http://localhost:3000/posts")
        .then(data=>{
            // data=data.json();
            if(data.length==0){
                let i=document.createElement("i");
                i.className="fa-solid fa-circle-exclamation";
                i.style.color="#e02410"
                i.style.fontSize="24px"
                let div=document.createElement("div");
                div.className="text-center mt-50 nostocks";
                div.appendChild(i);
                let h=document.createElement("h3");
                h.textContent="No Stocks Yet";
                div.appendChild(h);
                detail_container.appendChild(div);
            }
            else{
                console.log(data);
                let output="";
                const div=document.createElement("div");
                div.className="monish";
                data.forEach(post => {
                    output+=`
                    <div class='card mb-3'>
                    <div class='card-body grid'>
                    <h3 class='card-title upper'>${post.stockname}</h3>
                    <a href="#" class ='btn btn-success pull-right mr-2 buymore' data-post-id='${post.id}' data-toggle="modal" data-target="#editModal">Buy More</a>
                    <p class='card-text'>${post.stockquantity} Shares • Avg. \$  ${post.stockprice}</p>
                    <a href="#" class ='btn btn-danger delete pull-right mr-2 sell' data-post-id='${post.id}'>Sell</a>
                    </div>
                    </div>
                    `;
                });
                div.innerHTML=output;
                detail_container.appendChild(div);
            }
            
        })
        .catch(err=> console.warn(err));
        
    }
    showAlert(message,className){
        this.clearAlert();
        const div=document.createElement('div');
        
        div.className=className;
        
        div.appendChild(document.createTextNode(message));
        this.posts_container.insertBefore(div,this.posts_wrapper);
        
        setTimeout(()=>{
            this.clearAlert();
        },3000);
    }
    clearAlert(){
        const alertBox=document.querySelector('.alert');
        if(alertBox){
            alertBox.remove();
        }
    }
    clearFields(){
        this.stockquantity.value='';
        this.requiredbalance.value='';
        // this.author.value='';
    }
}
const ui= new UI();
// const http =new SlHTTP();
document.addEventListener("DOMContentLoaded",ui.showPosts());


document.querySelector('#detail-container').addEventListener('click', deletePost);
const BASE_URL="http://localhost:3000";
function deletePost(evt) {
    evt.preventDefault();
    console.log("delete me aaya");
    console.log(evt.target.classList);
    if(evt.target.classList.contains('delete')) {
        console.log("delete next")
        const postId = evt.target.dataset.postId;
        console.log(postId);
        const deletePostUrl = `${BASE_URL}/posts/${postId}`;
        http.get(deletePostUrl)
        .then(data=>{
            getlive(data.stockname,data.stockquantity);
        })
        .catch(err=>console.warn(err));
        setTimeout(() => {
            console.log("waiting")
            http.delete(deletePostUrl)
            .then(data => {
                console.log(data);
                showPosts();
            })
            .catch(err => console.warn(err));
        }, 2000);
        
        }
    }
